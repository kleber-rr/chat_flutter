import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyApp());
}

final googleSignIn = GoogleSignIn();
final auth = FirebaseAuth.instance; // este é um sigleton

Future<Null> _ensureLoggedIn() async {
  //dupla autenticacao
  // 1a no google
GoogleSignInAccount user = googleSignIn.currentUser;
  if (user == null)
    user = await googleSignIn
        .signInSilently(); //login silencioso (caso ele tenha feito anteriormente)
  if (user == null) user = await googleSignIn.signIn();

  // 2a no firebase
  if (await auth.currentUser() == null) {
    GoogleSignInAuthentication credentials =
        await googleSignIn.currentUser.authentication;
    await auth.signInWithCredential(GoogleAuthProvider.getCredential(
        idToken: credentials.idToken, accessToken: credentials.accessToken));
  }
}

_handlerSubmitted(String text) async {
  await _ensureLoggedIn();
  if (text != "") _sendMessage(texto: text);
}

void _sendMessage({String texto, String imageUrl}) {
  String idData = DateTime.now().millisecondsSinceEpoch.toString();
  Firestore.instance.collection("messages").document(idData).setData({
    "text": texto,
    "imgUrl": imageUrl,
    "senderName": googleSignIn.currentUser.displayName,
    "senderPhotoUrl": googleSignIn.currentUser.photoUrl
    // "dataTime": DateTime.now()
  });
}

final ThemeData kIOSTheme = ThemeData(
    primarySwatch: Colors.orange,
    primaryColor: Colors.grey[100],
    primaryColorBrightness: Brightness.light);

final ThemeData kDefaultTheme = ThemeData(
    primarySwatch: Colors.purple, accentColor: Colors.orangeAccent[400]);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "ChatOnline",
      debugShowCheckedModeBanner: false,
      theme: Theme.of(context).platform == TargetPlatform.iOS
          ? kIOSTheme
          : kDefaultTheme,
      home: ChatScreen(),
    );
  }
}

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  bool _result = false;

  @override
    void initState() {
      // verifica se o login foi realizado para prosseguir a lista
        _ensureLoggedIn().then((result){
          setState(() {
                _result = true;
            });
        });
    }

  @override
  Widget build(BuildContext context) {
    if (_result == false) {
            // This is what we show while we're loading
            return Center(
              child: CircularProgressIndicator(),
            );
        }
    return buildSafeArea(context);
  }

  SafeArea buildSafeArea(BuildContext context) {
    return SafeArea(
    //permite que se ignore o NOT do iphone e a barra inferior do iphoneX
    bottom: true,
    top: true,
    child: Scaffold(
      appBar: AppBar(
        title: Text("Chat"),
        centerTitle: true,
        elevation:
            Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: StreamBuilder(
              // atualiza os dados vindos do firebase em tela
              stream: Firestore.instance.collection("messages").snapshots(),
              builder: (context, snapshot)  {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  default:
                      
                      return buildListView(snapshot);
                   
                }
              },
            ),
          ),
          Divider(
            height: 1.0,
          ),
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
            ),
            child: TextComposer(),
          )
        ],
      ),
    ),
  );
  }

  ListView buildListView(AsyncSnapshot snapshot) {
    return ListView.builder(
      reverse: true,
      itemCount: snapshot.data.documents.length,
      itemBuilder: (context, index) {
        List r = snapshot.data.documents.reversed.toList();
        return ChatMessage(r[index].data);
      },
    );
  }

}

class TextComposer extends StatefulWidget {
  @override
  _TextComposerState createState() => _TextComposerState();
}

class _TextComposerState extends State<TextComposer> {
  final _textController = TextEditingController();
  bool _isComposing = false;

  void _reset() {
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(
            horizontal: 8.0), //margem não vai mudar com o const
        decoration: Theme.of(context).platform == TargetPlatform.iOS
            ? BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey[200])))
            : null,
        child: Row(
          children: <Widget>[
            Container(
              child: IconButton(
                icon: Icon(Icons.photo_camera),
                onPressed: () async {
                  await _ensureLoggedIn();
                  File imgFile = await ImagePicker.pickImage(source: ImageSource.gallery);
                  if(imgFile == null) return;
                  StorageUploadTask task = FirebaseStorage.instance.ref().
                    child(googleSignIn.currentUser.id.toString() + 
                      DateTime.now().millisecondsSinceEpoch.toString()).putFile(imgFile);
                  StorageTaskSnapshot storageTaskSnapshot = await task.onComplete;
                  String downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();
                  _sendMessage(imageUrl: downloadUrl.toString());
                }),
            ),
            Expanded(
              child: TextField(
                controller: _textController,
                decoration:
                    InputDecoration.collapsed(hintText: "Enviar uma mensagem"),
                onChanged: (text) {
                  setState(() {
                    _isComposing = text.length > 0;
                  });
                },
                onSubmitted: (text) {
                  _handlerSubmitted(text);
                  _reset();
                },
              ),
            ),
            Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? CupertinoButton(
                        child: Text("Enviar"),
                        onPressed: _isComposing
                            ? () {
                                _handlerSubmitted(_textController.text);
                                _reset();
                              }
                            : null,
                      )
                    : IconButton(
                        icon: Icon(Icons.send),
                        onPressed: _isComposing
                            ? () {
                                _handlerSubmitted(_textController.text);
                                _reset();
                              }
                            : null,
                      ))
          ],
        ),
      ),
    );
  }
}

class ChatMessage extends StatelessWidget {
  final Map<String, dynamic> data;

  ChatMessage(this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: googleSignIn.currentUser.displayName == data["senderName"]
          ? MyMessageRow(data: data)
          : OthersMessageRow(data: data),
    );
  }
}

class MyMessageRow extends StatelessWidget {
  const MyMessageRow({
    Key key,
    @required this.data,
  }) : super(key: key);

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                data["senderName"],
                style: Theme.of(context).textTheme.subhead,
              ),
              Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: data["imgUrl"] != null
                    ? Image.network(
                        data["imgUrl"],
                        width: 250.0,
                      )
                    : Text(data["text"]),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16.0),
          child: CircleAvatar(
            backgroundImage: NetworkImage(data["senderPhotoUrl"]),
          ),
        )
      ],
    );
  }
}

class OthersMessageRow extends StatelessWidget {
  const OthersMessageRow({
    Key key,
    @required this.data,
  }) : super(key: key);

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 16.0),
          child: CircleAvatar(
            backgroundImage: NetworkImage(data["senderPhotoUrl"]),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data["senderName"],
                style: Theme.of(context).textTheme.subhead,
              ),
              Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: data["imgUrl"] != null
                    ? Image.network(
                        data["imgUrl"],
                        width: 250.0,
                      )
                    : Text(data["text"]),
              )
            ],
          ),
        )
      ],
    );
  }
}
